/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

/**
 *
 * @author jayvy
 */
public class Beet extends Vegetable{
    
    public Beet(String color, double size){
        super(color, size);
        
        
    }
    public boolean isRipe(){
        
        if(color.equals("Red") && size == 2){
         return true;   
        }else{
            return false;
    }
    }
    
    
}
