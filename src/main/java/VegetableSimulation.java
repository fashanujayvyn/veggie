/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

/**
 *
 * @author jayvy
 */
import java.util.*;

public class VegetableSimulation {
    public static void main(String args[]){
        
        ArrayList<Vegetable> veg1 = new ArrayList<Vegetable>();
        
        Vegetable b1 = new Beet("Red", 2);
        Vegetable b2 = new Beet("Blue", 1);
Vegetable c1 = new Carrot("Orange", 1.5);
Vegetable c2 = new Carrot("Black", 4);

veg1.add(b1);
veg1.add(b2);
veg1.add(c1);
veg1.add(c2);

for(int i = 0; i < veg1.size(); i++){
    System.out.println(veg1.get(i).getColor());
    System.out.println(veg1.get(i).getSize());
    if(veg1.get(i).isRipe()){
        System.out.println("Is ripe");
}else{
        System.out.println("Is not ripe");
        
    }

}
    }
}
    
